#!/bin/bash
set -e
set -o pipefail
set +x

echo "AUTHENTICATING WITH GCLOUD..."
sudo /home/jenkins/google-cloud-sdk/bin/gcloud auth activate-service-account kubernetes-practice-us@kubernetes-practice-us.iam.gserviceaccount.com  --key-file=${service_account_json}
echo "AUTHENTICATION WAS SUCCESSFUL..."
echo "BUILDING DOCKER IMAGE..."
sudo /usr/bin/docker build -t gcr.io/kubernetes-practice-us/frontend:$(git rev-parse --short HEAD) -f docker/Dockerfile .
echo "DOCKER IMAGE BUILD SUCCESSFUL..."
sudo /home/jenkins/google-cloud-sdk/bin/gcloud config set project kubernetes-practice-us
echo "PROJECT ID SET SUCCESSFULLY..."
echo "PUSHING DOCKER IMAGE TO GCR..."
sudo /home/jenkins/google-cloud-sdk/bin/gcloud docker -- push gcr.io/kubernetes-practice-us/frontend:$(git rev-parse --short HEAD)
echo "DOCKER IMAGE PUSH TO GCR WAS SUCCESSFUL..."
echo "DELETING DOCKER IMAGE..."
sudo /usr/bin/docker rmi -f gcr.io/kubernetes-practice-us/backend:$(git rev-parse --short HEAD)
echo "DOCKER IMAGE DELETED SUCCESSFULLY...."