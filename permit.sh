#!/bin/bash
set -e
set -o pipefail
set +x

sudo chown -R $(whoami) /var/run
sudo chown -R $(whoami) /usr/local/lib/node_modules
sudo chown -R $(whoami) /usr/local/bin
sudo chown -R $(whoami) /usr/local/share
sudo chown -R $(whoami) /var/lib
sudo chown -R $(whoami) /var/cache
sudo chown -R $(whoami) /var/log/apt