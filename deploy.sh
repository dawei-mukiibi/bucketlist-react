#!/bin/bash
set -e
set -o pipefail
set +x

# echo "SETTING CONFIGS..."
sudo /home/jenkins/google-cloud-sdk/bin/gcloud config set compute/zone us-west1-a
sudo /home/jenkins/google-cloud-sdk/bin/gcloud config set container/cluster assessment-cluster
sudo /home/jenkins/google-cloud-sdk/bin/gcloud config set account david.mukiibi@andela.com
echo "DEPLOYING TO GKE..."
echo "AUTHENTICATING WITH GCLOUD..."
sudo /home/jenkins/google-cloud-sdk/bin/gcloud auth activate-service-account kubernetes-practice-us@kubernetes-practice-us.iam.gserviceaccount.com  --key-file=${service_account_json}
sudo /home/jenkins/google-cloud-sdk/bin/gcloud container clusters get-credentials assessment-cluster	 --zone=us-west1-a
echo "CHECKING IF DEPLOYMENT EXISTS..."
if sudo /home/jenkins/google-cloud-sdk/bin/kubectl get deployment/frontend; then
  echo "DEPLOYMENT EXISTS..."
  if sudo /home/jenkins/google-cloud-sdk/bin/gcloud container images describe gcr.io/kubernetes-practice-us/frontend:$(git rev-parse --short HEAD); then
    echo "UPDATING DEPLYMENT WITH NEWLY BUILT IMAGE..."
    sudo /home/jenkins/google-cloud-sdk/bin/kubectl set image deployment/frontend frontend=gcr.io/kubernetes-practice-us/frontend:$(git rev-parse --short HEAD)
    echo "UPDATE WAS SUCCESSFUL..."
  fi
fi
